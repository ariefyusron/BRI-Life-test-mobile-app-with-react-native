import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import Home from '../../home/screens/Home';
import Report from '../../home/screens/Report';

const stackNavigator = createStackNavigator({
  Home,
  Report
})

export default createAppContainer(stackNavigator);