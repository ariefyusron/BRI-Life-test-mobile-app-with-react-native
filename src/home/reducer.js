const initialState = {
  modalVisible: false,
  listPropinsi: [],
  listKontrasepsi: [],
  data: [],
  isLoading: false,
  isSuccess: false,
  isError: false,
  dataReport: [],
  heightArr: []
}

const homeReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_MODAL_VISIBLE':
      return {...state, modalVisible: action.payload}
    case 'GET_PROPINSI_PENDING':
      return {...state, isLoading: true}
    case 'GET_PROPINSI_FULFILLED':
      return {...state, listPropinsi: action.payload.data, isLoading: false}
    case 'GET_PROPINSI_REJECTED':
      return {...state, isLoading: false}
    case 'GET_KONTRASEPSI_PENDING':
      return {...state, isLoading: true}
    case 'GET_KONTRASEPSI_FULFILLED':
      return {...state, listKontrasepsi: action.payload.data, isLoading: false}
    case 'GET_KONTRASEPSI_REJECTED':
      return {...state, isLoading: false}
    case 'ADD_DATA_PENDING':
      return {...state, isLoading: true}
    case 'ADD_DATA_FULFILLED':
      state.data.unshift(action.payload.data)
      return {...state, isSuccess: true, isLoading: false, modalVisible: false}
    case 'ADD_DATA_REJECTED':
      return {...state, isLoading: false, isError: true}
    case 'GET_DATA_PENDING':
      return {...state, isLoading: true}
    case 'GET_DATA_FULFILLED':
      return {...state, data: action.payload.data, isLoading: false}
    case 'GET_DATA_REJECTED':
      return {...state, isLoading: false, isError: true}
    case 'GET_REPORT_PENDING':
      return {...state, isLoading: true}
    case 'GET_REPORT_FULFILLED':
      const data = action.payload.data
      let dataReport = []
      let heightArr = []
      let no = 1
      for(i in data){
        let initial = []
        initial.push(no)
        if(data[i].detailPemakai.length===0){
          data[i].detailPemakai.push({Jumlah: 0})
          data[i].detailPemakai.push({Jumlah: 0})
          data[i].detailPemakai.push({Jumlah: 0})
        } else if(data[i].detailPemakai.length===1){
          if(data[i].detailPemakai[0].Id_Kontrasepsi===1){
            data[i].detailPemakai.push({Jumlah: 0})
            data[i].detailPemakai.push({Jumlah: 0})
          } else if(data[i].detailPemakai[0].Id_Kontrasepsi===2){
            data[i].detailPemakai.unshift({Jumlah: 0})
            data[i].detailPemakai.push({Jumlah: 0})
          } else{
            data[i].detailPemakai.unshift({Jumlah: 0})
            data[i].detailPemakai.unshift({Jumlah: 0})
          }
        } else if(data[i].detailPemakai.length===2){
          if(data[i].detailPemakai[0].Id_Kontrasepsi===1 && data[i].detailPemakai[1].Id_Kontrasepsi===2){
            data[i].detailPemakai.push({Jumlah: 0})
          } else if(data[i].detailPemakai[0].Id_Kontrasepsi===1 && data[i].detailPemakai[1].Id_Kontrasepsi===3){
            data[i].detailPemakai.splice(1,0,{Jumlah: 0})
          } else{
            data[i].detailPemakai.unshift({Jumlah: 0})
          }
        }
        initial.push(data[i].Nama_Propinsi)
        initial.push(data[i].detailPemakai[0].Jumlah)
        initial.push(data[i].detailPemakai[1].Jumlah)
        initial.push(data[i].detailPemakai[2].Jumlah)
        initial.push(data[i].jumlahPemakai)
        no = no+1
        heightArr.push(40)
        dataReport.push(initial)
      }
      return {...state, dataReport, heightArr, isLoading: false}
    case 'GET_REPORT_REJECTED':
      return {...state, isLoading: false}
    default:
      return state;
  }
};

export default homeReducer;