import axios from 'axios';

const url = 'http://192.168.56.1:3000';

const setModalVisible = (visible) => ({
  type: 'SET_MODAL_VISIBLE',
  payload: visible
});

const getPropinsi = () => ({
  type: 'GET_PROPINSI',
  payload: axios.get(`${url}/propinsi`)
});

const getKontrasepsi = () => ({
  type: 'GET_KONTRASEPSI',
  payload: axios.get(`${url}/kontrasepsi`)
});

const addData = (value) => ({
  type: 'ADD_DATA',
  payload: axios.post(url, {
    Id_Propinsi: value.propinsi,
    Id_Kontrasepsi: value.kontrasepsi,
    Jumlah_Pemakai: value.jumlahPemakai
  })
});

const getData = () => ({
  type: 'GET_DATA',
  payload: axios.get(url)
});

const getReport = () => ({
  type: 'GET_REPORT',
  payload: axios.get(`${url}/report`)
});

export {
  setModalVisible,
  getPropinsi,
  getKontrasepsi,
  addData,
  getData,
  getReport
}