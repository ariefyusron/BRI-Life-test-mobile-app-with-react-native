import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, FlatList, Text } from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';

import { setModalVisible, getData } from '../action';
import ModalFormDataEntry from '../components/ModalFormDataEntry';
class Home extends Component {

  static navigationOptions = ({navigation}) => ({
    headerTitle: <Text style={{color: '#3498db', fontWeight: 'bold', fontSize: 18, marginLeft: 20}}>Keluarga Berencana</Text>,
    headerRight: (
      <TouchableOpacity style={{marginRight: 20}} onPress={() => navigation.navigate('Report')}>
        <Text style={{color: '#3498db'}}>Report</Text>
      </TouchableOpacity>
    )
  })

  componentDidMount() {
    this.props.dispatch(getData())
  }

  handleModalVisible(visible){
    this.props.dispatch(setModalVisible(visible))
  }

  _renderItem = ({item}) => (
    <View style={styles.containerItem}>
      <Text>Propinsi : {item.propinsi.Nama_Propinsi}</Text>
      <Text>Kontrasepsi : {item.kontrasepsi.Nama_Kontrasepsi}</Text>
      <Text>Jumlah Pemakai : {item.Jumlah_Pemakai}</Text>
    </View>
  )

  render() {
    return (
      <View style={styles.container}>
        <ModalFormDataEntry />
        <FlatList
          data={this.props.home.data}
          keyExtractor={(item) => item.id.toString()}
          style={styles.flatList}
          renderItem={this._renderItem}
        />
        <TouchableOpacity style={styles.button} onPress={() => this.handleModalVisible(true)}>
          <Icon name='plus' type='AntDesign' style={styles.iconButton} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F1F1F1'
  },
  button: {
    backgroundColor: '#3498db',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 25,
    right: 25
  },
  iconButton: {
    color: '#fff'
  },
  containerItem: {
    backgroundColor: '#fff',
    borderRadius: 5,
    marginVertical: 5,
    padding: 10
  },
  flatList: {
    paddingHorizontal: 10
  }
})

const mapStateToProps = (state) => ({
  home: state.home
})

export default connect(mapStateToProps)(Home);