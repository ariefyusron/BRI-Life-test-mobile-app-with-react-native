import React, { Component } from 'react';
import { Text } from 'react-native';
import { Table, TableWrapper, Row, Rows } from 'react-native-table-component';
import { connect } from 'react-redux';

import { getReport } from '../action';

class Report extends Component {
  constructor(){
    super()
    this.state = {
      tableHead: ['No','Propinsi','Pemakai alat Kontrasepsi','Jumlah'],
      tableSubHead: ['Pill','Kondom','IUD']
    }
  }

  static navigationOptions = {
    headerTitle: <Text style={{color: '#3498db', fontWeight: 'bold', fontSize: 18}}>Report</Text>
  }

  componentDidMount() {
    this.props.dispatch(getReport())
  }

  render() {
    return (
      <Table>
        <TableWrapper style={{flexDirection: 'row'}}>
          <Row data={['No','Propinsi']} style={{width: '30%'}} />
          <TableWrapper style={{width: '50%'}}>
            <Row data={['Pemakai alat Kontrasepsi']} />
            <Row data={['Pil','Kondom','IUD']} flexArr={[1,1,1]} />
          </TableWrapper>
          <Row data={['Jumlah']} style={{width: '20%'}} />
        </TableWrapper>
        <Rows data={this.props.home.dataReport} heightArr={this.props.home.heightArr} flexArr={[30/2,30/2,50/3,50/3,50/3,20]} />
      </Table>
    );
  }
}

const mapStateToProps = (state) => ({
  home: state.home
})

export default connect(mapStateToProps)(Report);