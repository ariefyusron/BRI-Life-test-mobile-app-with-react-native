import React, { Component } from 'react';
import { Picker, StyleSheet } from 'react-native';

class InputOptions extends Component {

  render() {
    const { input,data } = this.props
    return (
      <Picker
        selectedValue={input.value}
        style={styles.picker}
        onValueChange={input.onChange}
      >
        <Picker.Item label={input.name==='propinsi'? 'Pilih Propinsi':'Pilih Kontrasepsi'} value='' />
        {data.map(item => (
          <Picker.Item label={input.name==='propinsi'? item.Nama_Propinsi:item.Nama_Kontrasepsi} value={item.id} key={item.id.toString()} />
        ))}
      </Picker>
    );
  }
}

const styles = StyleSheet.create({
  picker: {
    height: 40
  }
})

export default InputOptions;