import React, { Component } from 'react';
import { TextInput, StyleSheet } from 'react-native';

class Input extends Component {

  render() {
    const { input, placeholder } = this.props

    return (
      <TextInput
        placeholder={placeholder}
        style={styles.input}
        onChangeText={input.onChange}
        value={input.value}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderRadius: 5,
    borderBottomColor: '#f0f0f0',
    borderBottomWidth: 1,
    height: 40
  }
})

export default Input;