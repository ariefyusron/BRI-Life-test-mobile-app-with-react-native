import React, { Component } from 'react';
import { Modal, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Spinner } from 'native-base';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { setModalVisible, getPropinsi, getKontrasepsi, addData } from '../action';
import Input from './redux-form/Input';
import InputOptions from './redux-form/InputOptions';

class ModalFormDataEntry extends Component {

  componentDidMount() {
    this.props.dispatch(getPropinsi())
    this.props.dispatch(getKontrasepsi())
  }

  handleModalVisible(visible){
    this.props.dispatch(setModalVisible(visible))
  }

  handleAdd = (value) => {
    this.props.dispatch(addData(value))
  }

  render() {
    return (
      <Modal
        animationType='fade'
        visible={this.props.home.modalVisible}
        transparent={true}
        onRequestClose={() => this.handleModalVisible(false)}
      >
        <View style={styles.container}>
          <View style={styles.content}>
            <View style={styles.header}>
              <TouchableOpacity onPress={() => this.handleModalVisible(false)}>
                <Text style={[styles.textButton, {color: '#e74c3c'}]}>Cancel</Text>
              </TouchableOpacity>
              <Text style={styles.titleHeader}>Add Data</Text>
              {this.props.home.isLoading? (
                <Spinner color='#dadada' />
              ):
                this.props.pristine? (
                  <Text style={[styles.textButton, {textAlign: 'right', color: '#dadada'}]}>Add</Text>
                ):(
                  <TouchableOpacity onPress={this.props.handleSubmit(this.handleAdd)}>
                    <Text style={[styles.textButton, {textAlign: 'right'}]}>Add</Text>
                  </TouchableOpacity>
                )
              }
            </View>
            <View style={styles.form}>
              <Field
                name='propinsi'
                data={this.props.home.listPropinsi}
                component={InputOptions}
              />
              <Field
                name='kontrasepsi'
                data={this.props.home.listKontrasepsi}
                component={InputOptions}
              />
              <Field
                name='jumlahPemakai'
                placeholder='Jumlah Pemakai'
                component={Input}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.1)'
  },
  content: {
    backgroundColor: '#fff',
    borderRadius: 5,
    width: '80%'
  },
  header: {
    borderBottomColor: '#ababab',
    borderBottomWidth: 0.5,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10
  },
  titleHeader: {
    fontWeight: 'bold'
  },
  textButton: {
    color: '#2ecc71',
    width: 50
  },
  form: {
    paddingHorizontal: 5
  }
})

const mapStateToProps = (state) => ({
  home: state.home
})

export default reduxForm({form: 'addData'})(connect(mapStateToProps)(ModalFormDataEntry));